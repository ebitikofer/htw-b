import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import registerServiceWorker from './registerServiceWorker';

import store from './store/store'

import App from './template/App';

import './index.css';

// store.subscribe(() => {
//   console.log("Store UPdates", store.getState());
// });

// store.dispatch({
//   type: 'ADD',
//   payload: 10,
// });

ReactDOM.render(
  <Provider store = { store } >
    <App/>
  </Provider>,
  document.getElementById('root')
);

registerServiceWorker();