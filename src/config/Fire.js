import firebase from 'firebase';

// Initialize Firebase
var config = {
  apiKey: "AIzaSyCRIDXucOt-PRnrJrnWoE9c_SI4WcsSyGM",
  authDomain: "toto-8e2b0.firebaseapp.com",
  databaseURL: "https://toto-8e2b0.firebaseio.com",
  projectId: "toto-8e2b0",
  storageBucket: "",
  messagingSenderId: "557282237140"
};

const fire = firebase.initializeApp(config); // Initialize the default app
//console.log(defaultApp.name);  // "[DEFAULT]"

// ... or you can use the equivalent shorthand notation
//defaultStorage = firebase.storage();
//defaultDatabase = firebase.database();

export default fire;
