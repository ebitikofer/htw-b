//core
import React, { Component } from 'react';
// import { connect } from 'react-redux';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faEnvelope, faKey, faCaretLeft, faArrowLeft,
  faRunning, faBook, faRuler, faLaptopCode, faCoins,
  faCodeBranch, faCode, faFileCode, faTerminal,
  faBug, faMicrochip, faServer, faDatabase,
  faMemory, faSave, faUserFriends, faSquareRootAlt,
  faUser, faUsers, faGraduationCap, faTasks,
  faUserShield, faExclamation, faUserTie, faUserSecret,
  faIdCard, faSeedling, faPaintBrush, faDragon,
  faPallet, faPalette, faGamepad, faCompactDisc,
  faTree, faLaptopMedical, faCompass, faBomb, faLink, faFolder } from '@fortawesome/free-solid-svg-icons';

// organisms
import Header from '../components/organisms/header/Header';
import Body from '../components/organisms/body/Body';
import Footer from '../components/organisms/footer/Footer';

//style
import './App.css';

library.add(
  faEnvelope, faKey, faCaretLeft, faArrowLeft,
  faRunning, faBook, faRuler, faLaptopCode,
  faCoins, faCodeBranch, faFileCode, faTerminal,
  faBug, faMicrochip, faServer, faDatabase,
  faMemory, faSave, faUserFriends, faSquareRootAlt,
  faUsers, faGraduationCap, faTasks, faUserShield,
  faExclamation, faUserTie, faUserSecret, faIdCard,
  faSeedling, faPaintBrush, faDragon, faPallet,
  faPalette, faGamepad, faCompactDisc, faTree,
  faLaptopMedical, faCompass, faBomb, faLink,
  faFolder,
);

class App extends Component {

  constructor(props) {

    super(props);
    this.updateDimensions = this.updateDimensions.bind(this)
    this.state = {
    };

  }

  componentDidMount() {
    const root = document.documentElement;
    // root.style.setProperty('--mouse-perc-x', 0);
    // root.style.setProperty('--mouse-perc-y', 0);
    root.style.setProperty('--mouse-x', 0);
    root.style.setProperty('--mouse-y', 0);
    document.addEventListener('mousemove', evt => {
        let x = evt.clientX / window.innerWidth;
        let y = evt.clientY / window.innerHeight;
        // root.style.setProperty('--mouse-perc-x', x);
        // root.style.setProperty('--mouse-perc-y', y);
        root.style.setProperty('--mouse-x', x - 0.5);
        root.style.setProperty('--mouse-y', y - 0.5);
        console.log(x - 0.5, y - 0.5);
        
    });

    window.addEventListener("resize", this.updateDimensions);

  }

  componentWillUnmount() {

    window.removeEventListener("resize", this.updateDimensions);

  }

  updateDimensions() {

    if(window.innerWidth < 500) {

      this.setState({ width: 450, height: 102 });

    } else {

      let updateWidth = window.innerWidth - 100;
      let updateHeight = Math.round(updateWidth / 4.4);
      this.setState({ width: updateWidth, height: updateHeight });

    }

    var baseNumber = Math.floor(window.innerWidth / 110.8);
    // console.log(Math.floor(baseNumber));

  }

  render() {
    return (

      <div className="App"><div class="circle"></div>
        <Header></Header>
        <Body></Body>
        <Footer></Footer>
      </div>

    );
  }

}

export default App;
