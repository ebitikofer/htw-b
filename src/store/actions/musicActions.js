export function setIndex(index) {
  return {
    type: 'SET_INDEX',
    payload: index
  };
}

export function setTime(time) {
  return {
    type: 'SET_TIME',
    payload: time
  };
}
