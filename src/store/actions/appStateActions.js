export function setHome(home) {
  return {
    type: 'HOME',
    payload: home
  };
}

export function setUser(user) {
  return {
    type: 'USER',
    payload: user
  };
}

export function setLogin(login) {
  return {
    type: 'LOGIN',
    payload: login
  };
}

export function setSubmit(submit) {
  return {
    type: 'SUBMIT',
    payload: submit
  };
}

export function setMusic(music) {
  return {
    type: 'MUSIC',
    payload: music
  };
}
