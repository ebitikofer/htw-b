export function setChatbot(chatbot) {
  return {
    type: 'SET_CHATBOT',
    payload: chatbot
  };
}
