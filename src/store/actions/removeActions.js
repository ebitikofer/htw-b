export function setRemove(remove) {
  return {
    type: 'REMOVE',
    payload: remove
  };
}
