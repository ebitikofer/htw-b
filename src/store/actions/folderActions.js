export function setRoot(root) {
  return {
    type: 'SET_ROOT',
    payload: root
  };
}

export function setCurrent(current) {
  return {
    type: 'SET_CURRENT',
    payload: current
  };
}
