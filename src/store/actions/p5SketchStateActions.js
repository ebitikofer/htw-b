export function setOne(one) {
  return {
    type: 'ONE',
    payload: one
  };
}

export function setTwo(two) {
  return {
    type: 'TWO',
    payload: two
  };
}

export function setThree(three) {
  return {
    type: 'THREE',
    payload: three
  };
}

export function setFour(four) {
  return {
    type: 'FOUR',
    payload: four
  };
}

export function setFive(five) {
  return {
    type: 'FIVE',
    payload: five
  };
}
