export function setPodcast(podcast) {
  return {
    type: 'SET_PODCAST',
    payload: podcast
  };
}
