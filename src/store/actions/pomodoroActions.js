export function setPomodoro(pomodoro) {
  return {
    type: 'POMODORO',
    payload: pomodoro
  };
}

export function setRest(rest) {
  return {
    type: 'SET_REST',
    payload: rest
  };
}
