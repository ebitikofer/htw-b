const restReducer = (state = {
  rest: false,
  pomodoro: false,
}, action) => {
  switch (action.type) {
    case 'POM_HOME':
      state = {
        ...state,
        pomodoro: false,
      }
      break;
    case 'POMODORO':
      state = {
        ...state,
        pomodoro: action.payload,
      }
      break;
    case 'SET_REST':
      state = {
        ...state,
        rest: action.payload,
      }
      break;
  }
  return state;
}

export default restReducer;
