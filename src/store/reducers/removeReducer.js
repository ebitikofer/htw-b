const removeReducer = (state = {
  remove: false,
}, action) => {
  switch (action.type) {
    case 'REMOVE':
      state = {
        ...state,
        remove: action.payload,
      }
      break;
  }
  return state;
}

export default removeReducer;
