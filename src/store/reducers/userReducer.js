const userReducer = (state = {
  email: 'e.bitikofer@gmail.com',
  name: 'Eric',
  id: 'VaUpCAVpVeVzcfMBD5Bh47kPOJI3',
}, action) => {
  switch (action.type) {
    case 'SET_EMAIL':
      state = {
        ...state,
        email: action.payload,
      }
      break;
    case 'SET_NAME':
      state = {
        ...state,
        name: action.payload,
      }
      break;
    case 'SET_ID':
      state = {
        ...state,
        id: action.payload,
      }
      break;
  }
  return state;
}

export default userReducer;
