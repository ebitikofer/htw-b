const musicReducer = (state = {
  index: 29,
  time: 1500,
}, action) => {
  switch (action.type) {
    case 'SET_INDEX':
    state = {
      ...state,
      index: action.payload,
    }
    break;
    case 'SET_TIME':
    state = {
      ...state,
      time: action.payload,
    }
    break;
  }
  return state;
}

export default musicReducer;
