const appStateReducer = (state = {
  user: false,
  login: false,
  submit: false,
  music: false
}, action) => {
  switch (action.type) {
    case 'HOME':
      state = {
        ...state,
        user: false,
        login: false,
        submit: false,
        music: false
      }
      break;
    case 'USER':
      state = {
        ...state,
        user: action.payload,
      }
      break;
    case 'LOGIN':
      state = {
        ...state,
        login: action.payload,
      }
      break;
    case 'SUBMIT':
      state = {
        ...state,
        submit: action.payload,
      }
      break;
    case 'MUSIC':
      state = {
        ...state,
        music: action.payload,
      }
      break;
  }
  return state;
}

export default appStateReducer;
