const appStateReducer = (state = {
  one: false,
  two: false,
  three: false,
  four: false,
  five: false,
}, action) => {
  switch (action.type) {
    case 'ONE':
      state = {
        ...state,
        one: action.payload,
      }
      break;
    case 'TWO':
      state = {
        ...state,
        two: action.payload,
      }
      break;
    case 'THREE':
      state = {
        ...state,
        three: action.payload,
      }
      break;
    case 'FOUR':
      state = {
        ...state,
        four: action.payload,
      }
      break;
    case 'FIVE':
      state = {
        ...state,
        five: action.payload,
      }
      break;
  }
  return state;
}

export default appStateReducer;
