const folderReducer = (state = {
  root: '-L8pM1jjW5zBc9RrriZz',
  current: '',
}, action) => {
  switch (action.type) {
    case 'SET_ROOT':
      state = {
        ...state,
        root: action.payload,
      }
      break;
    case 'SET_CURRENT':
      state = {
        ...state,
        current: action.payload,
      }
      break;
  }
  return state;
}

export default folderReducer;
