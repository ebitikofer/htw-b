import {
  createStore,
  combineReducers,
  applyMiddleware
} from 'redux';
import logger from 'redux-logger';

import appState from './reducers/appStateReducer';
import user from './reducers/userReducer';
import pomodoro from './reducers/pomodoroReducer'
import music from './reducers/musicReducer';
import folder from './reducers/folderReducer';
import remove from './reducers/removeReducer';
import podcast from './reducers/podcastReducer';
import youtube from './reducers/youtubeReducer';
import p5SketchState from './reducers/p5SketchStateReducer';

// const myLogger = (store) => (next) => (action) =>{
//   console.log('logged action:', action);
//   next(action);
// }

export default createStore(
  combineReducers({
    appState,
    user,
    pomodoro,
    music,
    folder,
    remove,
    podcast,
    youtube,
    p5SketchState
  }), {},
  // applyMiddleware(logger)
); //, logger()))