//core
import React, { Component } from 'react';
import { connect } from 'react-redux';

//components
import Nav from '../../molecules/nav/Nav';

// utility
import Animations from './Header.Animation';
import Animation from '../../utility/animations/Animation';

//style
import './Header.css';

class Header extends Component {

  constructor(props) {

    super(props);
    this.animations = new Animations();
    this.state = {
      classNames: 'header neumorph card-down',
      animations: new Animations(),
      leftScreen: false,
      offScreen: false,
      prevRest: this.props.pomodoro.rest,
      prevPom: this.props.pomodoro.pomodoro,
    }

  }

  render() {

    if (this.props.pomodoro.pomodoro !== this.state.prevPom) {
      this.setState({
        prevPom: this.props.pomodoro.pomodoro,
        leftScreen: false,
        offScreen: false,
      });
    }

    if (this.props.pomodoro.pomodoro) {

      if (this.props.pomodoro.rest !== this.state.prevRest) {
        this.setState({
          prevRest: this.props.pomodoro.rest,
          leftScreen: false,
          offScreen: false,
        });
      }

      if (this.props.pomodoro.rest) {
        if (this.state.leftScreen === false) {
          this.setState({
            leftScreen: true,
            animations: {
              ...this.state.animations,
              animation: {
                ...this.state.animations.animation,
                translateY: [0, -100],
                opacity: [1, 0]
              }
            }
          });
          setTimeout(function () {
            this.setState({
              offScreen: true,
              classNames: 'header neumorph card-down successColor',
              animations: {
                ...this.state.animations,
                animation: {
                  ...this.state.animations.animation,
                  translateY: [-100, 0],
                  opacity: [0, 1]
                }
              }
            });
          }.bind(this), 1000);
        }

      } else {

        if (this.state.leftScreen === false) {
          this.setState({
            leftScreen: true,
            animations: {
              ...this.state.animations,
              animation: {
                ...this.state.animations.animation,
                translateY: [0, -100],
                opacity: [1, 0]
              }
            }
          });
          setTimeout(function () {
            this.setState({
              offScreen: true,
              classNames: 'header neumorph card-down warningColor',
              animations: {
                ...this.state.animations,
                animation: {
                  ...this.state.animations.animation,
                  translateY: [-100, 0],
                  opacity: [0, 1]
                }
              }
            });
          }.bind(this), 1000);
        }

      }

    } else {

      if (this.state.leftScreen === false) {
        this.setState({
          leftScreen: true,
          animations: {
            ...this.state.animations,
            animation: {
              ...this.state.animations.animation,
              translateY: [0, -100],
              opacity: [1, 0]
            }
          }
        });
        setTimeout(function () {
          this.setState({
            offScreen: true,
            classNames: 'header neumorph card-down',
            animations: {
              ...this.state.animations,
              animation: {
                ...this.state.animations.animation,
                translateY: [-100, 0],
                opacity: [0, 1]
              }
            }
          });
        }.bind(this), 1000);
      }

    }

    return (

      <div className="header-container">
        <Animation animation={this.state.animations.animation}>
          <div className={ this.state.classNames }>
            <Nav offScreen={ this.state.offScreen } />
          </div>
        </Animation>
      </div>

    );

  }

}

const mapStateToProps = (state) => {
  return {
    pomodoro: state.pomodoro,
  };
};

const mapDispatchToProps = dispatch => {
  return {
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
