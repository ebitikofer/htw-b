import animationSettings from "../../utility/animations/models/animations";

class Animations {
    constructor() {

        this.animation = new animationSettings();

        // delay={0 * 62.5}

        this.animation.translateY = -100;
        this.animation.opacity = 0;
        this.animation.rotate = 0;
        this.animation.firstRun = false;
        this.animation.easing = "easeInOutExpo";
        this.animation.boxShadow = "";
        this.animation.firstRunAnimation = function () {
            this.animation.firstRun = true;
            // this.animation.translateY = 0;
            // this.animation.opacity = 1;
        }

    }
};

export default Animations;