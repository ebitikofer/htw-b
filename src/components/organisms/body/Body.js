//core
import React, { Component } from 'react';
// import { connect } from 'react-redux';
//components
import Links from '../../molecules/links/Links';
import Input from '../../molecules/input/Input';
// utility
import Animations from './Body.Animation';
// import Animation from '../../utility/animations/Animation';
//style
import './Body.css';

class Body extends Component {

  constructor(props) {

    super(props);
    this.animations = new Animations();

  }

  render() {

    return (
      
      <div className="bodyContainer">
        {/* <Animation animation={this.animations.animation}> */}
          <div className="body">
            <div className="links">
              <Links />
            </div>
            {/* <div className="form">
              <Input />
            </div> */}
          </div>
        {/* </Animation> */}
      </div>

    );
    
  }

}

export default Body;
