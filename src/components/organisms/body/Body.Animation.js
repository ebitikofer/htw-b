import animationSettings from "../../utility/animations/models/animations";

class Animations {
    constructor() {

        this.animation = new animationSettings();

    }
};

export default Animations;