//core
import React, { Component } from 'react';
// import { connect } from 'react-redux';
//components
import Music from '../../molecules/music/Music';
import Input from '../../molecules/input/Input';
// utility
import Animations from './Footer.Animation';
import Animation from '../../utility/animations/Animation';
//style
import './Footer.css';


class Footer extends Component {

  constructor(props) {

    super(props);

    this.animations = new Animations();

  }

  render() {

    return (
      
      <div className="footerContainer">
        <Animation animation={this.animations.animation}>
          <div className="footer neumorph card-full">
            {/* <Music /> */}
          </div>
          <div className="form">
            <Input />
          </div>
        </Animation>
      </div>

    );

  }

}

export default Footer;
