import animationSettings from "../../utility/animations/models/animations";

class Animations {
    constructor() {

        this.animation = new animationSettings();

        // delay={0 * 62.5}

        this.animation.translateY = [50, 0];
        this.animation.opacity = [0, 1];
        this.animation.rotate = 0;
        this.animation.firstRun = false;
        this.animation.easing = "easeInOutExpo";
        this.animation.firstRunAnimation = function () {
            this.animation.firstRun = true;
            this.animation.translateY = [[100, 0]];
            this.animation.opacity = [0, 1];
        }

    }
};

export default Animations;