//core
import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

//config
import Anime from '../../utility/animations/Anime';

//style
import './Link.css';

class Link extends Component {

  constructor(props) {

    super(props);
    this.state = {
      name: props.item.name,
      link: props.item.link,
      type: props.item.type,
      icon: props.item.icon,
      left: props.display.left,
      right: props.display.right,
      delay: props.display.delay,
      remove: props.display.remove
    };
    this.once = false;
    this.translate = 0;
    this.opacity = 1;

  }

  // componentDidUpdate() {
  //   if(!this.props.remove.remove) {
  //     if(this.once === false) {
  //       this.once = true;
  //       this.translate = [-25, 0];
  //       this.opacity = [0,1];
  //     }
  //   }
  // }

  render() {
    // create_ = (this.state.key, i, this.state.left, this.state.right) => {

    // if(this.once === false) {
    //   this.once = true;
    //   this.translate = [-25, 0];
    //   this.opacity = [0,1];
    // }

    if(this.once === false) {
      this.once = true;
      this.translate = [-25, 0];
      this.opacity = [0,1];
    }

    var styles = { background: '' };
    const del = { background: '#71dfbe' };
    const norm = { background: '#CF6679', color: '#000000', borderColor: '#ac033c' };
    if(remChk) { styles = del; } else { styles = norm; }

    // let keyChk = this.state.uls.hasOwnProperty(this.state.key);
    let keyChk = true;
    let lnkChk = this.state.type === "link";
    let remChk = this.state.remove;

    let word;
    let icon = null;

    if (this.state.icon !== null) {
      icon = <FontAwesomeIcon icon={this.state.icon}  />;
    } else {
      word = '<span id="a">' + this.state.name.charAt(0) + '</span>';
      for (var i = 1; i < this.state.name.length; i++) {
        word += '<span id="b">' + this.state.name.charAt(i) + '</span>';
      }
    }

    // console.log(word);

    if (keyChk) {
      var li = null;
      if(lnkChk) {
        if (icon === null) {
          if (remChk) {
            li = <a style={ styles } title={ this.state.name } onClick={ this.props.deleteFunc } dangerouslySetInnerHTML={{__html: word}} className={"neumorph card-full " + (this.state.left ? 'left-item ' : '') + (this.state.right ? 'right-item' : '')}></a>;
          } else {
            li = <a title={ this.state.name } href={ this.state.link } target="_blank" dangerouslySetInnerHTML={{__html: word}} className={"neumorph card-full " + (this.state.left ? 'left-item ' : '') + (this.state.right ? 'right-item' : '')}></a>;
          }
        } else {
          if (remChk) {
            li = <a style={ styles } title={ this.state.name } onClick={ this.props.deleteFunc } className={"neumorph card-full " + (this.state.left ? 'left-item ' : '') + (this.state.right ? 'right-item' : '')}>{icon}</a>;
          } else {
            li = <a title={ this.state.name } href={this.state.link} target="_blank" className={"neumorph card-full " + (this.state.left ? 'left-item ' : '') + (this.state.right ? 'right-item' : '')}>{icon}</a>;
          }
        }
      } else { //folder
        const c = this.state.link.toString();
        if (icon === null) {
          li = <a title={ this.state.name } onClick={this.props.folderFunc} dangerouslySetInnerHTML={{__html: word}} className={"neumorph card-full " + (this.state.left ? 'left-item ' : '') + (this.state.right ? 'right-item' : '')}></a>;
        } else {
          li = <a title={ this.state.name } onClick={this.props.folderFunc} className={"neumorph card-full " + (this.state.left ? 'left-item ' : '') + (this.state.right ? 'right-item' : '')}>{icon}</a>;
        }
      }

      return (
        <Anime easing="easeOutElastic"
          duration={1250} delay={this.state.delay * 100}
          translateY={this.translate} opacity={this.opacity}
          loop={false} autoplay={true} >
          <li>
            {li}
          </li>
        </Anime>
      );
    }
  }

}

export default Link;
