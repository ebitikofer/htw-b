//core
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Moment from 'react-moment';

//utility
import { flipFlop } from '../../utility/Circuits'

//config
import { setRest } from '../../../store/actions/pomodoroActions'

//style
import './Clock.css';

class Clock extends Component {

  constructor(props) {

    super(props);
    this.state = {
      date: new Date(),

      clock: {
        format: 'h:mm:ss A',
        interval: 1000
      },

      pomodoro: {
        start: '',
        stored: false
      },

      currentFormat: 'h:mm:ss A',
      pomodoroStart: '',
      timeSaved: false,
      formatSet: false,
      prevOffScreen: false,

      restOver: false,
      workOver: false
    };

  }

  componentDidMount() { }

  componentWillUnmount() { }

  render() {

    console.log((this.state.prevOffScreen === false) && (this.props.offScreen === true));

    flipFlop(
      this.props.militaryTime,
      this.state.formatSet,
      () => {
        this.setState({
          currentFormat: 'HH:mm:ss',
          formatSet: true
        });
      },
      () => {
        this.setState({
          currentFormat: 'h:mm:ss A',
          formatSet: false
        });
      }
    );

    if ((this.state.prevOffScreen === false) && (this.props.offScreen === true)) {
      flipFlop(
        this.props.pomodoro.pomodoro,
        this.state.timeSaved,
        () => {
          this.setState({
            pomodoroStart: new Date().toISOString(),
            timeSaved: true,
            currentFormat: 'h:mm:ss'
          }); 
        },
        () => {
          this.setState({
            pomodoroStart: '',
            timeSaved: false,
            currentFormat: 'h:mm:ss A'
          });
        }
      );

      if (this.state.restOver === true) {
        this.resetTimer();
        this.setState({
          restOver: false
        });
      }

      if (this.state.workOver === true) {
        this.resetTimer();
        this.setState({
          workOver: false
        });
      }
      
    }

    if (this.props.pomodoro.rest === true) {
      if (this.state.restOver === true) {
        this.props.setRest(false);
      }
    }

    if (this.props.pomodoro.rest === false) {
      if (this.state.workOver === true) {
        this.props.setRest(true);
      }
    }

    if (this.state.prevOffScreen !== this.props.offScreen) {
      this.setState({
        prevOffScreen: this.props.offScreen
      });
    }

    return (
      <div className="clock">
        {
          this.state.timeSaved === false ?
          <Moment
            onChange={
              (val) => {
              }
            }
            interval={ this.state.clock.interval }
            format={ this.state.currentFormat }
          /> :
          <Moment
            onChange={
              (val) => {
                let time = val.split(':');
                if (time.length === 2) {
                  if (this.props.pomodoro.rest === true) {
                    if (parseInt(time[1], 10) === 5) this.restOver();
                  } else {
                    if (parseInt(time[1], 10) === 5) this.workOver();
                  }
                }
              }
            }
            interval={ this.state.clock.interval }
            date={ this.state.pomodoroStart }
            durationFromNow
          />
        }
      </div>
    );
  }

  restOver() {
    this.setState({
      restOver: true
    });
  }

  workOver() {
    this.setState({
      workOver: true
    });
  }

  resetTimer() {
    this.setState({
      pomodoroStart: new Date().toISOString(),
      timeSaved: true,
      currentFormat: 'h:mm:ss'
    });
  }

}

const mapStateToProps = (state) => {
  return {
    pomodoro: state.pomodoro,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setRest: (rest) => {
      dispatch(setRest(rest));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Clock);
