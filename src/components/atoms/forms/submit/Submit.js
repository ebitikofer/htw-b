//core
import React, { Component } from 'react';
import { connect } from 'react-redux';
//3rd
import $ from "jquery";
import anime from 'animejs';
//config
import fire from '../../../../config/Fire';
import { setCurrent } from '../../../../store/actions/folderActions'
//style
import './Submit.css';

// function submit_and_reset() {
//     submit_(current);
//     document.getElementById("add_link").reset();
// }

class Submit extends Component {

  handleChangeLink = (event) => this.setState({link: event.target.value});
  handleChangeName = (event) => this.setState({name: event.target.value});
  handleChangeType = (event) => this.setState({type: event.target.checked});

  constructor(props) {
    super(props);
    this.state = {
      link: '',
      name: '',
      type: ''
    };
    this.submit = this.submit.bind(this);
  }

  submit(){
      var currentRef = fire.database().ref('folders/' + this.props.folder.current + '/links');
      // var x = document.getElementById("add_link");
      // this.setState({ link: x.elements[2].value });
      // this.setState({ name: x.elements[1].value });
      // this.setState({ type: x.elements[0].checked });
      console.log(this.state.type);
      var newLinkRef = null;
      if (this.state.type){ //folder
          var folderRef = fire.database().ref('folders');
          var newFolderRef = folderRef.push();
          newFolderRef.set({
              'parent': this.props.folder.current,
              'links': {
                  '1':{
                      'link': "https://ebitikofer.bitbucket.io",
                      'name': "h",
                      'type': "link"
                  }
              }
          });
          newLinkRef = currentRef.push();
          newLinkRef.set({
              'link': newFolderRef.key,
              'name': this.state.name,
              'type': "folder"
          });
      } else {
          newLinkRef = currentRef.push();
          newLinkRef.set({
              'link': this.state.link,
              'name': this.state.name,
              'type': "link"
          });
      }

      var pathEls = $(".check");
      for (var i = 0; i < pathEls.length; i++) {
        var pathEl = pathEls[i];
        var offset = anime.setDashoffset(pathEl);
        pathEl.setAttribute("stroke-dashoffset", offset);
      }

      anime({
        targets: pathEl,
      })

      var basicTimeline = anime.timeline({
        autoplay: false
      });

      basicTimeline.add({
        targets: ".text",
        duration: 1,
        opacity: "0"
      }).add({
        targets: ".button",
        duration: 1300,
        height: 20,
        width: 300,
        backgroundColor: "#2B2D2F",
        border: "0",
        borderRadius: 100
      }).add({
        targets: ".progress-bar",
        duration: 300,
        translateY: 20,
      }).add({
        targets: ".progress-bar",
        duration: 2000,
        width: 300,
        backgroundColor: "#71DFBE",
        easing: "easeInOutQuart"
      }).add({
        targets: ".button",
        width: 0,
        duration: 1
      }).add({
        targets: ".progress-bar",
        width: 80,
        height: 80,
        // translateY: 80,
        delay: 500,
        duration: 750,
        borderRadius: 80,
        backgroundColor: "#71DFBE"
      }).add({
        targets: pathEl,
        opacity: "1",
        strokeDashoffset: [offset, 0],
        duration: 200,
        easing: "easeInOutSine"
      }).add({
        targets: ".button",
        duration: 1,
        height: 80,
        width: 80,
        backgroundColor: "#71DFBE",
        border: "0",
        borderRadius: 100
      }).add({
        targets: ".progress-bar",
        duration: 1,
        translateY: 0,
        height: 20
      }).add({
        targets: ".progress-bar",
        duration: 250,
        opacity: "0",
        height: 20
      }).add({
        targets: pathEl,
        duration: 500,
        opacity: "0",
        easing: "easeInOutSine"
      }).add({
        targets: ".button",
        width: 200,
        duration: 750,
        // translateY: 0,
        backgroundColor: "#2B2D2F",
        easing: "easeInOutQuart"
      }).add({
        targets: ".text",
        duration: 500,
        opacity: "1"
      });

      basicTimeline.play();

  }

  render() {
    return (
      <div>
        <link href="https://fonts.googleapis.com/css?family=Poppins:600" rel="stylesheet">
        </link>
        <br />
        <div class="form" id="add_link">
          <div className="form-wrapper neumorph card-full">
            <div>
              <label class="container">
              <input type="checkbox" onChange={this.handleChangeType}/>
              <span class="checkmark"></span>
              </label>
              <input type="text" name="name" onChange={this.handleChangeName} className="form-control neumorph card-up" placeholder="name" ></input>
            </div>
            <input type="text" name="link" onChange={this.handleChangeLink} className="form-control neumorph card-down" placeholder="link"></input>
            <br />
            <div class="progress-bar"></div>
            <div class="button neumorph card-full" onClick={this.submit}>
              <div class="text">Submit</div>
            </div>
          </div>
        </div>
      </div>
    );
  }

//   <svg x="0px" y="0px" viewBox="0 0 25 30">
//   <path class="check" d="M2,19.2C5.9,23.6,9.4,28,9.4,28L23,2"></path>
//   </svg>

  // function submit_and_reset() {
  //     submit_(current);
  //     document.getElementById("add_link").reset();
  // }

}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    folder: state.folder,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setCurrent: (current) => {
      dispatch(setCurrent(current));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Submit);
