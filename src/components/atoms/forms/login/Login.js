//core
import React, { Component } from 'react';
import { connect } from 'react-redux';
//3rd
import $ from "jquery";
import anime from 'animejs';
//config
import fire from '../../../../config/Fire';
import { setLogin, setUser } from '../../../../store/actions/appStateActions'
//style
import './Login.css';


class Login extends Component {

  handleChangeEmail = (event) => this.setState({email: event.target.value});
  handleChangePassword = (event) => this.setState({password: event.target.value});

  constructor(props) {
    super(props);
    this.login = this.login.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.signup = this.signup.bind(this);
    this.state = {
      email: '',
      password: '',
      error: null,
    };
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  login(e) {

    e.preventDefault();
    fire.auth().signInWithEmailAndPassword(this.state.email, this.state.password).then((u)=>{
    }).catch((error) => {
        var errorCode = error.code;
        var errorMessage = error.message;
        if (errorCode === 'auth/wrong-password') {
          alert('Wrong password.');
        } else {
          alert(errorMessage);
        }
        console.log(error);
    });

    // this.props.setUser(true);
    this.props.setLogin(false);

    var pathEls = $(".check");
    for (var i = 0; i < pathEls.length; i++) {
      var pathEl = pathEls[i];
      var offset = anime.setDashoffset(pathEl);
      pathEl.setAttribute("stroke-dashoffset", offset);
    }

    anime({
      targets: pathEl,
    })

    var basicTimeline = anime.timeline({
      autoplay: false
    });

    basicTimeline.add({
      targets: ".text",
      duration: 1,
      opacity: "0"
    }).add({
      targets: ".button",
      duration: 1300,
      height: 20,
      width: 300,
      backgroundColor: "#2B2D2F",
      border: "0",
      borderRadius: 100
    }).add({
      targets: ".progress-bar",
      duration: 300,
      translateY: 20,
    }).add({
      targets: ".progress-bar",
      duration: 2000,
      width: 300,
      backgroundColor: "#71DFBE",
      easing: "easeInOutQuart"
    }).add({
      targets: ".button",
      width: 0,
      duration: 1
    }).add({
      targets: ".progress-bar",
      width: 80,
      height: 80,
      // translateY: 80,
      delay: 500,
      duration: 750,
      borderRadius: 80,
      backgroundColor: "#71DFBE"
    }).add({
      targets: pathEl,
      opacity: "1",
      strokeDashoffset: [offset, 0],
      duration: 200,
      easing: "easeInOutSine"
    }).add({
      targets: ".button",
      duration: 1,
      height: 80,
      width: 80,
      backgroundColor: "#71DFBE",
      border: "0",
      borderRadius: 100
    }).add({
      targets: ".progress-bar",
      duration: 1,
      translateY: 0,
      height: 20
    }).add({
      targets: ".progress-bar",
      duration: 250,
      opacity: "0",
      height: 20
    }).add({
      targets: pathEl,
      duration: 500,
      opacity: "0",
      easing: "easeInOutSine"
    }).add({
      targets: ".button",
      width: 200,
      duration: 750,
      // translateY: 0,
      backgroundColor: "#2B2D2F",
      easing: "easeInOutQuart"
    }).add({
      targets: ".text",
      duration: 500,
      opacity: "1"
    });

    basicTimeline.play();

  }

  signup(e){
    e.preventDefault();
    fire.auth().createUserWithEmailAndPassword(this.state.email, this.state.password).then((u)=>{
    }).then((u)=>{console.log(u)})
    .catch((error) => {
        console.log(error);
      });
    // this.props.setUser(true);
    this.props.setLogin(false);
  }

  render() {
    return (
      <div>
        <link href="https://fonts.googleapis.com/css?family=Poppins:600" rel="stylesheet">
        </link>
        <br />
        <div className="form" id="add_link">
          <div className="form-wrapper neumorph card-full">
            <input value={this.state.email} type="email" name="email" onChange={this.handleChange} className="form-control neumorph card-up" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="email" />
            <br />
            <input value={this.state.password} type="password" name="password" onChange={this.handleChange} className="form-control neumorph card-down" id="exampleInputPassword1" placeholder="password" />
            <br />
            <div className="progress-bar"></div>
            <div className="button neumorph card-full" onClick={this.login}>
              <div className="text">Login</div>
            </div>
          </div>
        </div>
      </div>
    );
    // <svg x="0px" y="0px" viewBox="0 0 25 30">
    //   <path className="check" d="M2,19.2C5.9,23.6,9.4,28,9.4,28L23,2"></path>
    // </svg>
    // <button onClick={this.signup} style={{marginLeft: '25px'}} className="btn btn-success">Signup</button>
  }
}

const mapStateToProps = (state) => {
  return {
    appState: state.appState,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setLogin: (login) => {
      dispatch(setLogin(login));
    },
    setUser: (user) => {
      dispatch(setUser(user));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
