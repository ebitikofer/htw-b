//core
import React, { Component } from 'react';
import { connect } from 'react-redux';
//3rd
import _ from 'lodash';
//config
import fire from '../config/Fire';
//style
import '../styles/Chatbot.css';
//utility
import ChatbotUtility from '../lib/ChatbotUtility.js';
//data
import { greetings, convo } from '../chatbot/data';
class Chatbot extends Component {

  constructor (props) {
    super(props);

    this.chatwrapper = React.createRef();
    this.letterpool = React.createRef();
    this.templetterpool = React.createRef();
    this.letteroverlay = React.createRef();
    this.chatmessagecolumnwrapper = React.createRef();
    this.chatmessagecolumn = React.createRef();
    this.messageinput = React.createRef();
    this.messageinputfield = React.createRef();
    this.chatbotmood = React.createRef();
    this.chatbotmoodvalue = React.createRef();

    this.state = {
      isUserSendingMessage: false,
      isChatBotSendingMessage: false,
      letterPool: {
        transitionPeriod: 30000,
        intervals: []
      },
      moods: ['friendly', 'suspicious', 'boastful'],
      currentMood: '',
      chatbotMessageIndex: 0,
      nLetterSets: 4,
      value: ''
    };

    this.resetTimeout = null;
    this.moodInterval = null;

    this.getRandMood = this.getRandMood.bind(this);
    this.setChatbotMood = this.setChatbotMood.bind(this);
    this.getRandGreeting = this.getRandGreeting.bind(this);
    this.getRandConvo = this.getRandConvo.bind(this);
    this.createLetter = this.createLetter.bind(this);
    this.getAlphabet = this.getAlphabet.bind(this);
    this.startNewLetterPath = this.startNewLetterPath.bind(this);
    this.setRandLetterPaths = this.setRandLetterPaths.bind(this);
    this.fillLetterPool = this.fillLetterPool.bind(this);
    this.findMissingLetters = this.findMissingLetters.bind(this);
    this.replenishLetterPool = this.replenishLetterPool.bind(this);
    this.clearLetterPool = this.clearLetterPool.bind(this);
    this.scrollToBottomOfMessages = this.scrollToBottomOfMessages.bind(this);
    this.checkMessageColumnHeight = this.checkMessageColumnHeight.bind(this);
    this.appendContentText = this.appendContentText.bind(this);
    this.createChatMessage = this.createChatMessage.bind(this);
    this.findLetterInPool = this.findLetterInPool.bind(this);
    this.createOverlayLetter = this.createOverlayLetter.bind(this);
    this.removePoolLetter = this.removePoolLetter.bind(this);
    this.setElPosFromRight = this.setElPosFromRight.bind(this);
    this.animateOverlayLetter = this.animateOverlayLetter.bind(this);
    this.animateMessageLetters = this.animateMessageLetters.bind(this);
    this.addChatMessage = this.addChatMessage.bind(this);
    this.clearInputField = this.clearInputField.bind(this);
    this.disableInputField = this.disableInputField.bind(this);
    this.enableInputField = this.enableInputField.bind(this);
    this.getChatbotMessageText = this.getChatbotMessageText.bind(this);
    this.sendChatbotMessage = this.sendChatbotMessage.bind(this);
    this.sendUserMessage = this.sendUserMessage.bind(this);
    this.onEnterPress = this.onEnterPress.bind(this);
    this.initLetterPool = this.initLetterPool.bind(this);
    this.resetLetterPool = this.resetLetterPool.bind(this);
    this.isValidLetter = this.isValidLetter.bind(this);
    this.canSendMessage = this.canSendMessage.bind(this);
    this.getRandMoodInterval = this.getRandMoodInterval.bind(this);
    this.setMoodInterval = this.setMoodInterval.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.handleKeyUp = this.handleKeyUp.bind(this);
    this.handleOnCut = this.handleOnCut.bind(this);
    this.onChange = this.onChange.bind(this);
    this.toggleInput = this.toggleInput.bind(this);
    this.checkIfInputFieldHasVal = this.checkIfInputFieldHasVal.bind(this)

  }

  componentDidMount () {

    window.onfocus = () => this.resetLetterPool();
    window.onresize = _.throttle(this.resetLetterPool, 200)

    this.setChatbotMood();
    this.initLetterPool();
    this.sendChatbotMessage();
    this.toggleInput();
    this.setMoodInterval(this.getRandMoodInterval());
    console.log("INITIALIZATION");

  }

  render() {

    return (

      <div className="chatbot_menu">
        <div id="chat-wrapper" ref={this.chatwrapper}>
          <div id="chat-bot-mood" ref={this.chatbotmood}>
            <div id="chat-bot-mood-icon" ref={this.chatbotmoodicon}></div>
            <div id="chat-bot-mood-label" ref={this.chatbotmoodlabel}>
              <h1 id="chat-bot-mood-text" ref={this.chatbotmoodtext}>Chatbot is feeling&nbsp;</h1>
              <h1 id="chat-bot-mood-value" ref={this.chatbotmoodvalue}>Mood</h1>
            </div>
          </div>
          <div id="letter-pool" ref={this.letterpool}></div>
          <div id="temp-letter-pool" ref={this.templetterpool}></div>
          <div id="letter-overlay" ref={this.letteroverlay}></div>
          <div id="chat-message-window" ref={this.chatmessagewindow}>
            <div id="message-input-wrapper" ref={this.messageinputwrapper}>
              <div id="message-input" ref={this.messageinput}>
                <input id="message-input-field" ref={this.messageinputfield} value={this.state.value} onKeyPress={this.handleKeyPress} onKeyUp={this.handleKeyUp} onCut={this.handleOnCut} onChange={this.onChange} type="text" name="inputfield" placeholder="Type a message" maxLength="100"/>
                <div id="send-message-button" ref={this.sendmessagebutton}><i className="fa fa-arrow-alt-circle-right"></i></div>
              </div>
            </div>
            <div className="scroll-bar" id="chat-message-column-wrapper" ref={this.chatmessagecolumnwrapper}>
              <div className="static" id="chat-message-column" ref={this.chatmessagecolumn}></div>
            </div>
          </div>
        </div>
      </div>

    );
  }

  getRandMood = () => {

    const rand = this.getRand(1, 3)
    return this.state.moods[rand - 1]

  }

  setChatbotMood = () => {
    this.state.currentMood = this.getRandMood()
    for(let i = 0; i < this.state.moods.length; i++){
      this.removeClass(this.chatbotmood.current, this.state.moods[i])
    }
    this.addClass(this.chatbotmood.current, this.state.currentMood)
    this.chatbotmoodvalue.current.innerHTML = this.state.currentMood
  }

  getRandGreeting = () => {
    let rand = 0
    switch(this.state.currentMood){
      case 'friendly':
        rand = this.getRand(1, greetings.friendly.length)
        return greetings.friendly[rand - 1]
      case 'suspicious':
        rand = this.getRand(1, greetings.suspicious.length)
        return greetings.suspicious[rand - 1]
      case 'boastful':
        rand = this.getRand(1, greetings.boastful.length)
        return greetings.boastful[rand - 1]
      default:
        break
    }
  }

  getRandConvo = () => {
    let rand = 0
    switch(this.state.currentMood){
      case 'friendly':
        rand = this.getRand(1, convo.friendly.length)
        return convo.friendly[rand - 1]
      case 'suspicious':
        rand = this.getRand(1, convo.suspicious.length)
        return convo.suspicious[rand - 1]
      case 'boastful':
        rand = this.getRand(1, convo.boastful.length)
        return convo.boastful[rand - 1]
      default:
        break
    }
  }

  createLetter = (cName, val) => {
    const letter = document.createElement('div')
    this.addClass(letter, cName)
    this.setAttr(letter, 'data-letter', val)
    letter.innerHTML = val
    return letter
  }

  getAlphabet = isUpperCase => {
    let letters = []
    for(let i = 65; i <= 90; i++){
      let val = String.fromCharCode(i),
            letter = null
      if(!isUpperCase) val = val.toLowerCase()
      letter = this.createLetter('pool-letter', val)
      letters.push(letter)
    }
    return letters
  }

  startNewLetterPath = (letter, nextRand, interval) => {
    clearInterval(interval)
    nextRand = this.getRandExcept(1, 4, nextRand)
    let nextPos = this.getRandPosOffScreen(nextRand),
            transitionPeriod = this.state.letterPool.transitionPeriod,
            delay = this.getRand(0, this.state.letterPool.transitionPeriod),
            transition = `left ${transitionPeriod}ms linear ${delay}ms, top ${transitionPeriod}ms linear ${delay}ms, opacity 0.5s`
    this.setElPos(letter, nextPos.x, nextPos.y)
    this.setStyle(letter, 'transition', transition)
    interval = setInterval(() => {
      this.startNewLetterPath(letter, nextRand, interval)
    }, this.state.letterPool.transitionPeriod + delay)
    this.state.letterPool.intervals.push(interval)
  }

  setRandLetterPaths = letters => {
    for(let i = 0; i < letters.length; i++){
      let letter = letters[i],
            startRand = this.getRand(1, 4),
            nextRand = this.getRandExcept(1, 4, startRand),
            startPos = this.getRandPosOffScreen(startRand),
            nextPos = this.getRandPosOffScreen(nextRand),
            transitionPeriod = this.state.letterPool.transitionPeriod,
            delay = this.getRand(0, this.state.letterPool.transitionPeriod) * -1,
            transition = `left ${transitionPeriod}ms linear ${delay}ms, top ${transitionPeriod}ms linear ${delay}ms, opacity 0.5s`

      this.setElPos(letter, startPos.x, startPos.y)
      this.setStyle(letter, 'transition', transition)
      this.addClass(letter, 'invisible')
      this.letterpool.current.appendChild(letter)
      setTimeout(() => {
        this.setElPos(letter, nextPos.x, nextPos.y)
        this.removeClass(letter, 'invisible')
        let interval = setInterval(() => {
          this.startNewLetterPath(letter, nextRand, interval)
        }, this.state.letterPool.transitionPeriod + delay)
      }, 1)
    }
  }

  fillLetterPool = (nSets = 1) => {
    for(let i = 0; i < nSets; i++){
      const lCaseLetters = this.getAlphabet(false),
            uCaseLetters = this.getAlphabet(true)
      this.setRandLetterPaths(lCaseLetters)
      this.setRandLetterPaths(uCaseLetters)
    }
  }

  findMissingLetters = (letters, lCount, isUpperCase) => {
    let missingLetters = []
    for(let i = 65; i <= 90; i++){
      let val = isUpperCase ? String.fromCharCode(i) : String.fromCharCode(i).toLowerCase(),
          nLetter = letters.filter(letter => letter === val).length
      if(nLetter < lCount){
        let j = nLetter
        while(j < lCount){
          missingLetters.push(val)
          j++
        }
      }
    }
    return missingLetters
  }

  replenishLetterPool = (nSets = 1) => {
    const poolLetters = this.letterpool.current.childNodes
    let charInd = 65,
        currentLetters = [],
        missingLetters = [],
        lettersToAdd = []

    for(let i = 0; i < poolLetters.length; i++){
      currentLetters.push(poolLetters[i].dataset.letter)
    }
    missingLetters = [...missingLetters, ...this.findMissingLetters(currentLetters, nSets, false)]
    missingLetters = [...missingLetters, ...this.findMissingLetters(currentLetters, nSets, true)]
    for(let i = 0; i < missingLetters.length; i++){
      const val = missingLetters[i]
      lettersToAdd.push(this.createLetter('pool-letter', val))
    }
    this.setRandLetterPaths(lettersToAdd)
  }

  clearLetterPool = () => {
    this.removeAllChildren(this.letterpool.current)
  }

  scrollToBottomOfMessages = () => {
    this.chatmessagecolumnwrapper.current.scrollTop = this.chatmessagecolumnwrapper.current.scrollHeight
  }

  checkMessageColumnHeight = () => {
    if(this.chatmessagecolumn.current.clientHeight >= window.innerHeight){
      this.removeClass(this.chatmessagecolumn.current, 'static')
    }
    else{
      this.addClass(this.chatmessagecolumn.current, 'static')
    }
  }

  appendContentText = (contentText, text) => {
    for(let i = 0; i < text.length; i++){
      const letter = document.createElement('span')
      letter.innerHTML = text[i]
      this.setAttr(letter, 'data-letter', text[i])
      contentText.appendChild(letter)
    }
  }

  createChatMessage = (text, isReceived) => {
    let message = document.createElement('div'),
        profileIcon = document.createElement('div'),
        icon = document.createElement('i'),
        content = document.createElement('div'),
        contentText = document.createElement('h1'),
        direction = isReceived ? 'received' : 'sent'

    this.addClass(content, 'content')
    this.addClass(content, 'invisible')
    this.addClass(contentText, 'text')
    this.addClass(contentText, 'invisible')
    this.appendContentText(contentText, text)
    content.appendChild(contentText)

    this.addClass(profileIcon, 'profile-icon')
    this.addClass(profileIcon, 'invisible')
    profileIcon.appendChild(icon)

    this.addClass(message, 'message')
    this.addClass(message, direction)

    if(isReceived){
      this.addClass(icon, 'fab')
      this.addClass(icon, 'fa-cloudsmith')
      this.addClass(message, this.state.currentMood)
      message.appendChild(profileIcon)
      message.appendChild(content)
    }
    else{
      this.addClass(icon, 'far')
      this.addClass(icon, 'fa-user')
      message.appendChild(content)
      message.appendChild(profileIcon)
    }

    return message
  }

  findLetterInPool = targetLetter => {
    let letters = this.letterpool.current.childNodes,
          foundLetter = null
    for(let i = 0; i < letters.length; i++){
      const nextLetter = letters[i]
      if(nextLetter.dataset.letter === targetLetter && !nextLetter.dataset.found){
        foundLetter = letters[i];
        this.setAttr(foundLetter, 'data-found', true);
        break;
      }
    }
    return foundLetter
  }

  createOverlayLetter = val => {
    const overlayLetter = document.createElement('span')
          this.addClass(overlayLetter, 'overlay-letter')
          this.addClass(overlayLetter, 'in-flight')
          overlayLetter.innerHTML = val
    return overlayLetter
  }

  removePoolLetter = letter => {
    this.addClass(letter, 'invisible')
    setTimeout(() => {
      this.removeChild(this.letterpool.current, letter)
    }, 500)
  }

  setElPosFromRight = (el, x, y) => {
    this.setStyle(el, 'right', x + 'px')
    this.setStyle(el, 'top', y + 'px')
  }

  animateOverlayLetter = (letter, contentText, finalPos, isReceived) => {
    this.removePoolLetter(letter)
    const initPos = letter.getBoundingClientRect(),
          overlayLetter = this.createOverlayLetter(letter.dataset.letter)
    if(isReceived){
      this.setElPos(overlayLetter, initPos.left, initPos.top)
    }
    else{
      this.setElPosFromRight(overlayLetter, window.innerWidth - initPos.right, initPos.top)
    }
    this.letteroverlay.current.appendChild(overlayLetter)
    setTimeout(() => {
      if(isReceived){
        this.setElPos(overlayLetter, finalPos.left, finalPos.top)
      }
      else{
        this.setElPosFromRight(overlayLetter, window.innerWidth - finalPos.right, finalPos.top)
      }
      setTimeout(() => {//asdf
        this.removeClass(contentText, 'invisible')
        this.addClass(overlayLetter, 'invisible')
        setTimeout(() => {
          this.removeChild(this.letteroverlay.current, overlayLetter)
        }, 1000)
      }, 1500)
    }, 100)
  }

  animateMessageLetters = (message, isReceived) => {
    const content = message.getElementsByClassName('content')[0],
          contentText = content.getElementsByClassName('text')[0],
          letters = contentText.childNodes,
          textPos = contentText.getBoundingClientRect()
    for(let i = 0; i < letters.length; i++){
      const letter = letters[i],
            targetLetter = this.findLetterInPool(letter.dataset.letter),
            finalPos = letter.getBoundingClientRect()
      if(targetLetter){
        this.animateOverlayLetter(targetLetter, contentText, finalPos, isReceived)
      }
      else{
        const tempLetter = this.createLetter('teter', letter.dataset.letter),
              pos = this.getRandPosOffScreen()
        this.addClass(tempLetter, 'invisible')
        this.setElPos(tempLetter, pos.x, pos.y)
        this.templetterpool.current.appendChild(tempLetter)
        this.animateOverlayLetter(tempLetter, contentText, finalPos, isReceived)
        setTimeout(() => {
          this.removeChild(this.templetterpool.current, tempLetter)
        }, 100)
      }
    }
  }

  addChatMessage = (text, isReceived) => {
    const message = this.createChatMessage(text, isReceived),
          content = message.getElementsByClassName('content')[0],
          contentText = content.getElementsByClassName('text')[0],
          profileIcon = message.getElementsByClassName('profile-icon')[0]
    this.chatmessagecolumn.current.appendChild(message)
    this.toggleInput()
    setTimeout(() => {
      this.removeClass(profileIcon, 'invisible')
      setTimeout(() => {
        this.removeClass(content, 'invisible')
        setTimeout(() => {
          this.animateMessageLetters(message, isReceived)
          setTimeout(() => this.replenishLetterPool(this.state.nLetterSets), 2500)
        }, 1000)
      }, 250)
    }, 250)
  }

  clearInputField = () => {
    this.state.value = ''
  }

  disableInputField = () => {
    this.messageinputfield.current.blur()
    this.state.value = ''
    this.messageinputfield.current.readOnly = true
  }

  enableInputField = () => {
    this.messageinputfield.current.readOnly = false
    this.messageinputfield.current.focus()
  }

  getChatbotMessageText = () => {
    if(this.state.chatbotMessageIndex === 0){
      return this.getRandGreeting()
    }
    else{
      return this.getRandConvo()
    }
  }

  sendChatbotMessage = () => {
    const text = this.getChatbotMessageText()
    this.state.isChatBotSendingMessage = true
    this.addChatMessage(text, true)
    this.state.chatbotMessageIndex++
    setTimeout(() => {
      this.state.isChatBotSendingMessage = false
      this.toggleInput()
    }, 4000)
  }

  sendUserMessage = () => {
    const text = this.state.value
    this.state.isUserSendingMessage = true
    this.addChatMessage(text, false)
    setTimeout(() => {
      this.state.isUserSendingMessage = false
      this.toggleInput()
    }, 4000)
  }

  onEnterPress = e => {
    this.sendUserMessage()
    setTimeout(() => {
      this.sendChatbotMessage()
    }, 4000)
    this.toggleInput()
    this.clearInputField()
  }

  initLetterPool = () => {
    this.clearLetterPool()
    this.fillLetterPool(this.state.nLetterSets)
  }

  resetLetterPool = () => {
    const intervals = this.state.letterPool.intervals
    for(let i = 0; i < intervals.length; i++){
      clearInterval(intervals[i])
    }
    clearTimeout(this.resetTimeout)
    this.clearLetterPool()
    this.resetTimeout = setTimeout(() => {
      this.initLetterPool()
    }, 200)
  }

  isValidLetter = e => {
    return !e.ctrlKey
      && e.key !== 'Enter'
      && e.keyCode !== 8
      && e.keyCode !== 9
      && e.keyCode !== 13
  }

  canSendMessage = () => !this.state.isUserSendingMessage && !this.state.isChatBotSendingMessage

  getRandMoodInterval = () => this.getRand(20000, 40000)

  setMoodInterval = time => {
    this.moodInterval = setInterval(() => {
      clearInterval(this.moodInterval)
      this.setChatbotMood()
      this.setMoodInterval(this.getRandMoodInterval())
    }, time)
  }

  handleKeyPress = e => {

    if (this.checkIfInputFieldHasVal() && e.key === 'Enter') {

      this.removeClass(this.messageinput.current, 'send-enabled');

      if (this.canSendMessage()) {

        this.onEnterPress(e);

      }

    }

  }

  handleKeyUp = () => {

    this.toggleInput();

  }

  handleOnCut = () => {

    this.toggleInput();

  }

  onChange = e => {

    this.setState({ value: e.target.value });

  }

  toggleInput = () => {

    if (this.checkIfInputFieldHasVal() && this.canSendMessage()) {

      this.addClass(this.messageinput.current, 'send-enabled')

    } else {

      this.removeClass(this.messageinput.current, 'send-enabled')

    }

  }

  checkIfInputFieldHasVal = () => {

    this.state.value.length > 0;

  }

  ///////////
  // UTILS //
  ///////////

  //
  setStyle = (el, prop, val) => {

    el.style[prop] = val;

  }

  setAttr = (el, attr, val) => el.setAttribute(attr, val);

  hasClass = (el, className) => el.classList.contains(className);

  addClass = (el, className) => {
    if(!this.hasClass(el, className))
      el.classList.add(className)
  }

  removeClass = (el, className) => {
    if(this.hasClass(el, className))
      el.classList.remove(className)
  }

  resetStyles = el => el.removeAttribute('style')

  removeChild = (el, child) => {
    if(child.parentNode === el) el.removeChild(child)
  }

  removeAllChildren = el => {
    while (el.hasChildNodes()) el.removeChild(el.lastChild)
  }

  getElPos = el => {
    const offset = el.getBoundingClientRect()
    return {
      left: offset.left,
      top: offset.top
    }
  }

  setElPos = (el, x, y) => {
    this.setStyle(el, 'left', x + 'px')
    this.setStyle(el, 'top', y + 'px')
  }

  getRand = (min, max) => Math.floor(Math.random() * max) + min

  getRandExcept = (min, max, exception) => {
    const rand = this.getRand(min, max)
    return rand === exception ? this.getRandExcept(min, max, exception) : rand
  }

  getRandPosOffScreen = overrideQuadrant => {
    const lowX1 = 0 - (window.innerWidth * 0.2),
          highX1 = 0 - (window.innerWidth * 0.1),
          lowY1 = 0,
          highY1 = window.innerHeight,

          lowX2 = window.innerWidth * 1.1,
          highX2 = window.innerWidth * 1.2,
          lowY2 = 0,
          highY2 = window.innerHeight,

          lowX3 = 0,
          highX3 = window.innerWidth,
          lowY3 = 0 - (window.innerHeight * 0.2),
          highY3 = 0 - (window.innerHeight * 0.1),

          lowX4 = 0,
          highX4 = window.innerWidth,
          lowY4 = window.innerHeight * 1.1,
          highY4 = window.innerHeight * 1.2

    let rand = Math.floor((Math.random() * 4) + 1)

    if(overrideQuadrant){
      rand = overrideQuadrant
    }

    let x = 0,
        y = 0

    switch(rand){
      case 1:
        x = Math.floor(Math.random() * (highX1 - lowX1 + 1)) + lowX1
        y = Math.floor(Math.random() * (highY1 - lowY1)) + lowY1
        break
      case 2:
        x = Math.floor(Math.random() * (highX2 - lowX2 + 1)) + lowX2
        y = Math.floor(Math.random() * (highY2 - lowY2)) + lowY2
        break
      case 3:
        x = Math.floor(Math.random() * (highX3 - lowX3 + 1)) + lowX3
        y = Math.floor(Math.random() * (highY3 - lowY3)) + lowY3
        break
      case 4:
        x = Math.floor(Math.random() * (highX4 - lowX4 + 1)) + lowX4
        y = Math.floor(Math.random() * (highY4 - lowY4)) + lowY4
        break
    }

    return { x, y }
  }

  resetAllTimeouts = () => {
    let id = window.setTimeout(() => {}, 0)
    while(id--) {
      window.clearTimeout(id)
    }
  }

  resetAllIntervals = () => {
    let id = window.setInterval(() => {}, 0)
    while(id--) {
      window.clearInterval(id)
    }
  }

}




export default Chatbot;
