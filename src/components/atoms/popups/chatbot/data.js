export const greetings = {

  friendly: [
    "Hiya, pal. I hope you're having a terrific day!",
    "Good day to you, friend!"
  ],

  suspicious: [
    "Hmm, I would introduce myself, but I'm not so sure thats a good idea.",
    "Hello, how are you? Wait, don't answer that, I have no way of verifying your response!"
  ],

  boastful: [
    "Hey, did I mention I am built on JavaScript? Which is the greatest language ever by the way!",
    "Good day to you. Though I must say that I am having a GREAT day!"
  ]

}

export const convo = {

  friendly: [
    "What a great thing you just said. I'm so glad you said it.",
    "Ahh, yes, I agree. It is so great to say things, isn't it?",
    "Please, tell me more. It brings me such joy to respond to the things you say.",
    "Ahh, yes valid point. Or was it? Either way, you're fantastic!",
    "Anyways, did I mention that I hope you're having a great day? If not, I hope it gets better!"
  ],

  suspicious: [
    "I just don't know if I can trust that thing you just said...",
    "Oh, interesting. I totally believe you. (Not really)",
    "Uh-huh, yeah, listen...I'm not going to fully invest in this conversation until I'm certain I know your motive.",
    "Wait, what the heck is that?? Oh, phewf, it's just another rogue letter 'R' that escaped the letter pool.",
    "You can't fool me, I know that's not true!"
  ],

  boastful: [
    "That's interesting. I'll have you know that I have an extremely advanced learning algorithm that analyzes everything you say...well, not really, but I wish.",
    "Hey, while I have you, I should probably tell you that I can respond in 4 seconds flat. Which is pretty fast if you ask me.",
    `Listen, that's neat and all, but look how fast I can calculate this math problem: 12345 * 67890 = ${12345 * 67890}. Didn't even break a sweat.`,
    "Oh, I forgot to mention that I've existed for over 100,000 seconds and that's something I'm quite proud of.",
    "Wow, thats pretty cool, but I can hold my breath for all of eternity. And it took me 0 seconds to gain that ability."
  ]
  
}
