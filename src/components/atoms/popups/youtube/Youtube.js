//core
import React, { Component } from 'react';
import { connect } from 'react-redux';
//3rd
//config
// import fire from '../config/Fire';
import { setIndex, setTime } from '../../../../store/actions/musicActions'
//style
import './Youtube.css';


class Youtube extends Component {

  render() {

      const yt = 'https://www.youtube.com/';
      const embd = 'embed/videoseries';
      const id = '?list=' + 'PLcI5DcePWMN0vZJUf5W4E_LW3e5ghyWGA';
      const sttng = '&amp;showinfo=0&amp;rel=0&amp;autoplay=1';
      const strt = '&amp;start=' + Math.ceil(Math.random() * this.props.music.time).toString();
      const indx = '&amp;index=' + Math.ceil(Math.random() * (this.props.music.index - 1)).toString();

      const src = yt + embd + id + sttng + strt + indx;

    return (
      <div className="youtube_menu popup neumorph card-full">
        <iframe title="music" src={src} frameBorder="0" allow="autoplay; encrypted-media" height="100%" width="100%" allowFullScreen>mus</iframe>
      </div>
    );
    // <button onClick={this.signup} style={{marginLeft: '25px'}} className="btn btn-success">Signup</button>
  }
}


const mapStateToProps = (state) => {
  return {
    music: state.music,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setIndex: (index) => {
      dispatch(setIndex(index));
    },
    setTime: (time) => {
      dispatch(setTime(time));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Youtube);
