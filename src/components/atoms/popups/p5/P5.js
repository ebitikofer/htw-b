//core
import React, { Component } from 'react';
import { connect } from 'react-redux';
//3rd
import P5Wrapper from 'react-p5-wrapper';
//config
// import fire from '../config/Fire';
import { setOne, setTwo, setThree, setFour, setFive } from '../../../../store/actions/p5SketchStateActions'
//style
import './P5.css';
//sketches
import sketch5 from './p5sketches/sketch1';
import sketch4 from './p5sketches/sketch2';
import sketch3 from './p5sketches/sketch3';
import sketch2 from './p5sketches/sketch4';
import sketch1 from './p5sketches/sketch5';
//images
import arrowIcon from '../../../../res/arrow_music.svg';

class P5 extends Component {

  constructor(props) {
    super(props);
    this.state = {
      rotation: 1,
      resolution: 50,
      angle: 1,
      stateSketch: sketch1,
    };
    this.p5Click1 = this.p5Click1.bind(this);
    this.p5Click2 = this.p5Click2.bind(this);
    this.p5Click3 = this.p5Click3.bind(this);
    this.p5Click4 = this.p5Click4.bind(this);
    this.p5Click5 = this.p5Click5.bind(this);
  }

  rotationChange(e){
    this.setState({rotation:e.target.value});
  }

  resolutionChange(e){
    this.setState({resolution:e.target.value});
  }

  angleChange(e){
    this.setState({angle:e.target.value});
  }

  // pressEvent(){
  //   this.state.stateSketch === sketch ? this.setState({stateSketch:sketch2}) : this.setState({stateSketch:sketch});
  // }

  render() {

      // const yt = 'https://www.youtube.com/';
      // const embd = 'embed/videoseries';
      // const id = '?list=' + 'PLcI5DcePWMN0vZJUf5W4E_LW3e5ghyWGA';
      // const sttng = '&amp;showinfo=0&amp;rel=0&amp;autoplay=1';
      // const strt = '&amp;start=' + Math.ceil(Math.random() * this.props.music.time).toString();
      // const indx = '&amp;index=' + Math.ceil(Math.random() * (this.props.music.index - 1)).toString();
      //
      // const src = yt + embd + id + sttng + strt + indx;

      // <button onClick={this.pressEvent.bind(this)}>Change Sketch</button>

    return (
      <div>
        <div className="p5_menu popup neumorph card-full">
          <div className="sketch_arrow_1">
            <div className="pad-div">
              <img src={arrowIcon} className="Input-arrow" alt="arrow" onClick={ this.p5Click1 }/><br/>
            </div>
          </div>
          <div className="sketch_arrow_2">
            <div className="pad-div">
              <img src={arrowIcon} className="Input-arrow" alt="arrow" onClick={ this.p5Click2 }/><br/>
            </div>
          </div>
          <div className="sketch_arrow_3">
            <div className="pad-div">
              <img src={arrowIcon} className="Input-arrow" alt="arrow" onClick={ this.p5Click3 }/><br/>
            </div>
          </div>
          <div className="sketch_arrow_4">
            <div className="pad-div">
              <img src={arrowIcon} className="Input-arrow" alt="arrow" onClick={ this.p5Click4 }/><br/>
            </div>
          </div>
          <div className="sketch_arrow_5">
            <div className="pad-div">
              <img src={arrowIcon} className="Input-arrow" alt="arrow" onClick={ this.p5Click5 }/><br/>
            </div>
          </div>
          <P5Wrapper sketch={this.state.stateSketch} rotation={this.state.rotation} resolution={this.state.resolution} angle={this.state.angle} /><br />
        </div>
        <div className="p5_controls">
          <input type="range" value={this.state.rotation}  min="0.78539816339"  max="1.0471975512" step="0.00000001" onInput={this.rotationChange.bind(this)}/><br />
          <input type="range" value={this.state.resolution}  min="6.25"  max="100" step="6.25" onInput={this.resolutionChange.bind(this)}/><br />
          <input type="range" value={this.state.angle} min="0.52359877559"  max="1.0471975512" step="0.01" onInput={this.angleChange.bind(this)}/>
        </div>
      </div>
    );

    // <button onClick={this.signup} style={{marginLeft: '25px'}} className="btn btn-success">Signup</button>

  }

  p5Click1() {
    this.props.setOne(!this.props.p5SketchState.one);
    this.setState({ stateSketch: sketch1 });
  };

  p5Click2() {
    this.props.setTwo(!this.props.p5SketchState.two);
    this.setState({ stateSketch: sketch2 });
  };

  p5Click3() {
    this.props.setThree(!this.props.p5SketchState.three);
    this.setState({ stateSketch: sketch3 });
  };

  p5Click4() {
    this.props.setFour(!this.props.p5SketchState.four);
    this.setState({ stateSketch: sketch4 });
  };

  p5Click5() {
    this.props.setFive(!this.props.p5SketchState.five);
    this.setState({ stateSketch: sketch5 });
  };

}


const mapStateToProps = (state) => {
  return {
    p5SketchState: state.p5SketchState,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setOne: (one) => {
      dispatch(setOne(one));
    },
    setTwo: (two) => {
      dispatch(setTwo(two));
    },
    setThree: (three) => {
      dispatch(setThree(three));
    },
    setFour: (four) => {
      dispatch(setFour(four));
    },
    setFive: (five) => {
      dispatch(setFive(five));
    },

  };
};

export default connect(mapStateToProps, mapDispatchToProps)(P5);
