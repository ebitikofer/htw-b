// Coding Rainbow
// Daniel Shiffman
// http://patreon.com/codingtrain
// Code for: https://youtu.be/0jjeOYMjmDU

export default function sketch (p) {

  let rotation = 100;
  let resolution = 0;
  let mult = 1;

  var angle = 0;
  var length = 0;
  var width = 0;
  var red = 0;
  var green = 0;
  var blue = 0;

  var circles;

  function Circle(x, y) {
    this.x = x;
    this.y = y;
    this.r = 1;
    this.growing = true;

    this.grow = function() {
      if (this.growing) {
        this.r += 1;
      }
    }

    this.show = function() {
      p.stroke(255);
      p.noFill();

      p.strokeWeight(2);
      p.ellipse(this.x, this.y, this.r * 2, this.r * 2);
    }

    this.edges = function() {
      return (this.x + this.r >= p.width || this.x - this.r <= 0 || this.y + this.r >= p.height || this.y - this.r <= 0)
    }
  }

  p.setup = function () {
    p.createCanvas(400, 400);
    circles = [];
  };

  p.myCustomRedrawAccordingToNewPropsHandler = function (props) {
    if (props.rotation){
      rotation = props.rotation;
    }
    if (props.resolution){
      resolution = props.resolution;
    }
    if (props.angle){
      mult = props.angle;
    }
  };

  function newCircle() {
    var x = p.random(p.width);
    var y = p.random(p.height);
    var valid = true;
    for (var i = 0; i < circles.length; i++) {
      var circle = circles[i];
      var d = p.dist(x, y, circle.x, circle.y);
      if (d < circle.r) {
        valid = false;
        break;
      }
    }
    if (valid) {
      return new Circle(x, y);
    } else {
      return null;
    }
  }

  p.draw = function () {
    p.background(0);
    p.frameRate(20)

    var total = 5;
    var count = 0;
    var attempts = 0;

    while (count < total) {
      var newC = newCircle();
      if (newC !== null) {
        circles.push(newC);
        count++;
      }
      attempts++;
      if (attempts > 100) {
        p.noLoop();
        console.log("finished");
        break;
      }
    }

    for (var i = 0; i < circles.length; i++) {
      var circle = circles[i];

      if (circle.growing) {
        if (circle.edges()) {
          circle.growing = false;
        } else {
          for (var j = 0; j < circles.length; j++) {
            var other = circles[j];
            if (circle !== other) {
              var d = p.dist(circle.x, circle.y, other.x, other.y);
              var distance = circle.r + other.r;

              if (d - 2 < distance) {
                circle.growing = false;
                break;
              }
            }
          }
        }
      }

      circle.show();
      circle.grow();
    }
  };

};
