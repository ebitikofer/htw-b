// Coding Rainbow
// Daniel Shiffman
// http://patreon.com/codingtrain
// Code for: https://youtu.be/0jjeOYMjmDU

export default function sketch (p) {

  let rotation = 100;
  let resolution = 0;
  let mult = 1;

  var angle = 0;
  var length = 0;
  var width = 0;
  var red = 0;
  var green = 0;
  var blue = 0;

  var stars = [];
  var speed;
  // var rspeed;

  function Star() {
    this.x = p.random(-p.width, p.width);
    this.y = p.random(-p.height, p.height);
    this.z = p.random(p.width);
    this.pz = this.z;

    this.update = function() {
      this.z = this.z - speed;
      if (this.z < 1) {
        this.z = p.width;
        this.x = p.random(-p.width, p.width);
        this.y = p.random(-p.height, p.height);
        this.pz = this.z;
      }
    }

    this.show = function() {
      p.fill(255);
      p.noStroke();

      var sx = p.map(this.x / this.z, 0, 1, 0, p.width);
      var sy = p.map(this.y / this.z, 0, 1, 0, p.height);

      var r = p.map(this.z, 0, p.width, 16, 0);
      p.ellipse(sx, sy, r, r);

      var px = p.map(this.x / this.pz, 0, 1, 0, p.width);
      var py = p.map(this.y / this.pz, 0, 1, 0, p.height);

      this.pz = this.z;

      p.stroke(255);
      p.line(px, py, sx, sy);

    }
  }

  p.setup = function () {
    p.createCanvas(400, 400);
    for (var i = 0; i < 800; i++) {
      stars[i] = new Star();
    }
    //p.slider = createSlider(0, Math.PI / 3, Math.PI / 4, 0.01);
  };

  p.myCustomRedrawAccordingToNewPropsHandler = function (props) {
    if (props.rotation){
      rotation = props.rotation;
    }
    if (props.resolution){
      resolution = props.resolution;
    }
    if (props.angle){
      mult = props.angle;
    }
  };

  p.draw = function () {
    p.background(51);
    angle = p.map(rotation, 0.78539816339, 1.04719755120, 0, Math.PI / 3);//slider.value();
    length = p.map(angle, 0, Math.PI / 3, 0, 100); //(slider.value(), 0, PI / 3, 0, 100);
    width = p.map(length, 0, 100, 0, 5);
    red = p.map(length, 0, 100, 0, 200);
    green = p.map(length, 0, 100, 255, 0);
    blue = p.map(length, 0, 100, 0, 255);
    p.stroke(red, green, blue);
    // p.translate(0, 200);

    speed = p.map(rotation, 0.78539816339, 1.04719755120, 0, 50);
    // rspeed = p.random(0, 50);
    // rspeed = p.random(speed - 10, speed + 10);
    // speed = p.map(p.mouseX, 0, p.width, 0, 50);
    p.background(0);
    p.translate(p.width / 2, p.height / 2);
    for (var i = 0; i < stars.length; i++) {
      stars[i].update();
      stars[i].show();
    }
  };

};
