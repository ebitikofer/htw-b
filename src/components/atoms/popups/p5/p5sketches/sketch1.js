// Coding Rainbow
// Daniel Shiffman
// http://patreon.com/codingtrain
// Code for: https://youtu.be/0jjeOYMjmDU

export default function sketch (p) {
  let rotation = 0;
  let resolution = 0;
  let mult = 1;

  var angle = 0;
  var length = 0;
  var width = 0;
  var red = 0;
  var green = 0;
  var blue = 0;

  p.setup = function () {
    p.createCanvas(400, 400, p.WEBGL);
    //p.slider = createSlider(0, Math.PI / 3, Math.PI / 4, 0.01);
  };

  p.myCustomRedrawAccordingToNewPropsHandler = function (props) {
    if (props.rotation){
      rotation = props.rotation;
    }
    if (props.resolution){
      resolution = props.resolution;
    }
    if (props.angle){
      mult = props.angle;
    }
  };

  p.draw = function () {
    p.background(51);
    angle = p.map(rotation, 0.78539816339, 1.04719755120, 0, Math.PI / 3);//slider.value();
    length = p.map(angle, 0, Math.PI / 3, 0, 100); //(slider.value(), 0, PI / 3, 0, 100);
    width = p.map(length, 0, 100, 0, 5);
    red = p.map(length, 0, 100, 0, 200);
    green = p.map(length, 0, 100, 255, 0);
    blue = p.map(length, 0, 100, 0, 255);
    p.stroke(red, green, blue);
    p.translate(0, 200);
    branch(length, width, red, green, blue);
  };

  function branch(len, wid, r, g, b) {
    r = p.map(len, 0, 100, 255, 0);
    g = p.map(len, 0, 100, 0, 255);
    b = p.map(len, 0, 100, 255, 0);
    p.stroke(r, g, b);
    p.strokeWeight(wid);
    p.line(0, -len, 0, 0);
    p.translate(0, -len);
    if (len > resolution) {
      p.push();
      p.rotateZ(angle * mult);
      branch(len * 0.67, wid * 0.67, r * 0.67, g * 0.67, b * 0.67);
      p.pop();
      p.push();
      p.rotateZ(-angle * mult);
      branch(len * 0.67, wid * 0.67, r * 0.67, g * 0.67, b * 0.67);
      p.pop();
    }
  };

};
