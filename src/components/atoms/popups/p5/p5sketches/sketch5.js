// Coding Rainbow
// Daniel Shiffman
// http://patreon.com/codingtrain
// Code for: https://youtu.be/0jjeOYMjmDU

export default function sketch (p) {
  let rotation = 0;
  let resolution = 0;
  let mult = 1;

  var angle = 0;
  var length = 0;
  var width = 0;
  var red = 0;
  var green = 0;
  var blue = 0;

  var n = 0;
  var c = 3;
  var w = 4;
  var h = 4;
  var rot = 0;

  var points = [];

  var start = 0;

  p.disableFriendlyErrors = true; // disables FES

  p.setup = function () {
      p.createCanvas(400, 400);
      p.angleMode(p.DEGREES);
      p.colorMode(p.HSB);
      p.background(0);
      n = 200;
  };

  // p.myCustomRedrawAccordingToNewPropsHandler = function (props) {
  //   if (props.rotation){
  //     rotation = props.rotation;
  //   }
  //   if (props.resolution){
  //     resolution = props.resolution;
  //   }
  //   if (props.angle){
  //     mult = props.angle;
  //   }
  // };

  p.draw = function () {
    // p.background(0);
    p.translate(p.width / 2, p.height / 2);
    // p.rotate(n * 0.3);
    if(n <= 400) {
      for (var i = 0; i < n; i++) {
        var a = i * 137.5;
        var r = c * p.sqrt(i);
        var x = r * p.cos(a) * 0.5;
        var y = r * p.sin(a) * 0.5;
        //var hu = p.sin(start + i * 0.5);
        //hu = p.map(hu, -1, 1, 0, 360);
        p.fill(50, 255, 255);
        // p.noStroke();
        // p.rotate(rot);
        p.ellipse(x, y, w, h);
      }
      n += 10;
    } else if(n < 450) {
      for (var i = 380; i < n; i++) {
        var a = i * 137.5;
        var r = c * p.sqrt(i);
        r += 40;
        var x = r * p.cos(a) * 0.5;
        var y = r * p.sin(a) * 0.5;
        w = 10;
        h = 40;
        p.fill(50, 255, 255);
        // p.noStroke();
        p.push();
          p.translate(x, y);
          rot = i * 137.5 + 90;
          p.rotate(rot);
          p.ellipse(0, 0, w, h);
        p.pop();
      }
      n += 1;
    }
    // start += 5;
  };

};
