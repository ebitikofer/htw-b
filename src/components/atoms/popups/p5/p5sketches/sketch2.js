export default function sketch (p) {
  let rotation = 0;
  let resolution = 0;
  let mult = 1;

  var angle = 0;
  var length = 0;
  var width = 0;
  var red = 0;
  var green = 0;
  var blue = 0;

  p.setup = function () {
    p.createCanvas(400, 400, p.WEBGL);
  };

  p.myCustomRedrawAccordingToNewPropsHandler = function (props) {
    if (props.rotation){
      rotation = props.rotation;
    }
    if (props.resolution){
      resolution = props.resolution;
    }
    if (props.angle){
      mult = props.angle;
    }
  };

  p.draw = function () {
    p.background(51);
    angle = p.map(rotation, 0.78539816339, 1.04719755120, 0, Math.PI / 3);//slider.value();
    length = p.map(angle, 0, Math.PI / 3, 0, 100); //(slider.value(), 0, PI / 3, 0, 100);
    width = p.map(length, 0, 100, 0, 5);
    red = p.map(length, 0, 100, 0, 200);
    green = p.map(length, 0, 100, 255, 0);
    blue = p.map(length, 0, 100, 0, 255);
    p.stroke(red, green, blue);
    // p.translate(0, 200);
    p.background(100);
    p.scale(0.25);
    p.noStroke();

    // p.push();
    // p.translate(-150, 100);
    // p.rotateY(rotation);
    // p.rotateX(-0.9);
    // p.box(100);
    // p.pop();

    // p.strokeWeight(angle);
    p.noFill();
    p.stroke(255);
    p.push();
    p.strokeWeight(resolution * 0.255);
    p.rotateY(rotation * 0.8 * 360);
    p.rotateX(rotation * 0.5 * 360);
    p.rotateZ(rotation * 360);
    // p.translate(500, p.height*0.35, -200);
    p.sphere(mult * 540);
    p.pop();
  };
};
