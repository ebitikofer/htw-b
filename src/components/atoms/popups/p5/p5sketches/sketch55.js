// Coding Rainbow
// Daniel Shiffman
// http://patreon.com/codingtrain
// Code for: https://youtu.be/0jjeOYMjmDU

var angle = 0;
var length = 0;
var width = 0;
var red = 0;
var green = 0;
var blue = 0;
var slider;

function setup() {
  createCanvas(400, 400);
  slider = createSlider(0, PI / 3, PI / 4, 0.01);
}

function draw() {
  background(51);
  angle = slider.value();
  length = map(slider.value(), 0, PI / 3, 0, 100);
  width = map(length, 0, 100, 0, 5);
  red = map(length, 0, 100, 0, 200);
  green = map(length, 0, 100, 255, 0);
  blue = map(length, 0, 100, 0, 255);
  stroke(red, green, blue);
  translate(200, height);
  branch(length, width, red, green, blue);
}

function branch(len, wid, r, g, b) {
  r = map(len, 0, 100, 255, 0);
  g = map(len, 0, 100, 0, 255);
  b = map(len, 0, 100, 255, 0);
  stroke(r, g, b);
  strokeWeight(wid);
  line(0, 0, 0, -len);
  translate(0, -len);
  if (len > 4) {
    push();
    rotate(angle);
    branch(len * 0.67, wid * 0.67, r * 0.67, g * 0.67, b * 0.67);
    pop();
    push();
    rotate(-angle);
    branch(len * 0.67, wid * 0.67, r * 0.67, g * 0.67, b * 0.67);
    pop();
  }
}
