//core
import React, { Component } from 'react';
//config
import Anime from '../../utility/animations/Anime';
//style
import './Tab.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretDown } from '@fortawesome/free-solid-svg-icons';

class Tab extends Component {

  constructor(props) {

    super(props);
    this.tabClick = this.tabClick.bind(this);

  }

  render() {

    this.props.arrow.translateX = window.innerWidth - ( 20 + ( this.props.index * 55 ) ) - 35;
    this.props.arrow.delay = ( 20 + ( this.props.index * 2 ) ) * 62.5;

    if (this.props.active) {
      this.props.arrow.rotate = 0;
    } else {
      this.props.arrow.rotate = 180;
    }

    if (this.props.arrow.firstRun === false) {
      this.props.arrow.firstRun = true;
      this.props.arrow.height = 20;
      this.props.arrow.opacity = [0, 1];
    } else {
      this.props.arrow.delay = 62.5;
      this.props.arrow.opacity = 1;
    }

    return (
      <div className="tabRow">
        <Anime
          easing="easeInOutExpo"
          translateX={this.props.arrow.translateX}
          delay={this.props.arrow.delay}
          duration={1000}
        >
          <div className="tabContainer">
            <Anime
              easing="easeOutElastic"
              duration={1000}
              delay={this.props.arrow.delay}
              height={this.props.arrow.height}
            >
              <div className="arrow neumorph card-up" onClick={this.tabClick}>
                <Anime
                  easing="easeOutElastic"
                  rotate={this.props.arrow.rotate}
                  duration={1000}
                  delay={this.props.arrow.delay}
                  translateY={this.props.arrow.translateY}
                  opacity={this.props.arrow.opacity}
                >
                  <div className="tab">
                    <FontAwesomeIcon
                      icon={faCaretDown}
                      onClick={this.tabClick}
                    />
                  </div>
                </Anime>
              </div>
            </Anime>
          </div>
        </Anime>
      </div>
    );
  }

  tabClick() {
    this.props.setActive();
  }

}

export default Tab;
