//core
import React, { Component } from 'react';
import { connect } from 'react-redux';
//config
import { setMusic } from '../../../store/actions/appStateActions';
//forms
import Login from '../../atoms/forms/login/Login';
import Submit from '../../atoms/forms/submit/Submit';
import Podcast from '../../atoms/popups/podcast/Podcast';
import P5 from '../../atoms/popups/p5/P5';
import Youtube from '../../atoms/popups/youtube/Youtube';
import Chatbot from '../../atoms/popups/chatbot/Chatbot';
import Tab from '../../atoms/tab/Tab';
//style
import './Input.css';
//images
// import arrowIcon from '../../res/arrow_music.svg';
import Animations from './Input.Animations';
import Animation from '../../utility/animations/Animation';

class Input extends Component {

  constructor(props) {
    super(props);

    this.popupTemplates = [
      <Podcast />,
      <P5 />,
      <Youtube />,
      <Chatbot />
    ];

    let popupStates = new Array(this.popupTemplates.length);
    for (let i = 0; i < popupStates.length; i++) {
      popupStates[i] = false;
    }

    this.state = { 
      height: window.innerHeight,
      width: window.innerWidth,
      popupState: popupStates
    };

    this.animations = new Animations();

    this.tabClick = this.tabClick.bind(this);

    const debouncedHandleResize = this.debounce(function handleResize() {
      this.setState({ 
        height: window.innerHeight,
        width: window.innerWidth
      })
    }, 100)
  
    window.addEventListener("resize", debouncedHandleResize);

  }

  render() {
    var form = null;

    if (this.props.appState.login) {
      form = <Login />;
    } else if (this.props.appState.submit) {
      form = <Submit />;
    }

    return (
      <div>

        {this.popupTemplates.map(function(template, index) {
          return <Animation animation={this.animations.popup}>{this.state.popupState[index] ? this.popupTemplates[index] : null}</Animation>;
        }.bind(this))}

        <Animation animation={this.animations.form}>
          <div className="inputForm">{form}</div>
        </Animation>

        {this.popupTemplates.map(function(template, index) {
          return <Tab arrow={this.animations.arrow} index={index} setActive={() => this.tabClick(index)} active={this.state.popupState[index]} />;
        }.bind(this))}

      </div>
    );
  }

  tabClick(index) {
    // open this tab
    let temp = this.state.popupState;
    temp[index] = !this.state.popupState[index];
    this.setState({
      popupStat: temp
    })
    // close other tabs
    // this.props.setMusic(!this.props.appState.music);
  }

  debounce(fn, ms) {
    let timer
    return _ => {
      clearTimeout(timer)
      timer = setTimeout(_ => {
        timer = null
        fn.apply(this, arguments)
      }, ms)
    };
  }
  
}

const mapStateToProps = state => {
  return {
    appState: state.appState
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setMusic: music => {
      dispatch(setMusic(music));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Input);
