import animationSettings from "../../utility/animations/models/animations";

class Animations {
    constructor() {

        this.popup = new animationSettings();
        this.arrow = new animationSettings();
        this.form = new animationSettings();

        this.popup.translateY = [100, 0];
        this.popup.opacity = [0, 1];
        this.popup.rotate = 180;
        this.popup.firstRun = false;
        this.popup.easing = "easeOutElastic";
        this.popup.firstRunAnimation = function () {
            this.props.popup.firstRun = true;
            this.props.popup.translateY = [[100, 0]];
            this.props.popup.opacity = [0, 1];
        }

        this.arrow.translateX = 0;
        this.arrow.translateY = 0;
        this.arrow.opacity = 1;
        this.arrow.rotate = 180;
        this.arrow.firstRun = false;
        this.arrow.easing = "easeOutElastic";
        this.arrow.height = [0, 20];

        this.form.translateY = [100, 0];
        this.form.opacity = [0, 1];
        this.form.rotate = 180;
        this.form.firstRun = false;
        this.form.easing = "easeOutElastic";
    }
};

export default Animations;