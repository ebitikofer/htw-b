//core
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

//components
import Link from '../../atoms/link/Link';

//config
import fire from '../../../config/Fire';
import Anime from '../../../components/utility/animations/Anime';
import { setCurrent } from '../../../store/actions/folderActions'

//style
import './Links.css';

class Links extends Component {

  constructor(props) {

    super(props);
    this.delete_ = this.delete_.bind(this);
    this.print_ = this.print_.bind(this);
    this.current = this.props.folder.root;
    
    this.state = {
      listItems: null,
      width:  800,
      height: 182
    };

    this.once = false;
    this.translate = 0;
    this.opacity = 1;

  }

  componentDidMount() {

    this.print_(this.props.folder.root);
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions.bind(this));

  }

  componentWillUnmount() {

    window.removeEventListener("resize", this.updateDimensions.bind(this));

  }

  updateDimensions() {

    if(window.innerWidth < 500) {

      this.setState({ width: 450, height: 102 });

    } else {

      let updateWidth = window.innerWidth - 100;
      let updateHeight = Math.round(updateWidth / 4.4);
      this.setState({ width: updateWidth, height: updateHeight });

    }

    var baseNumber = Math.floor(window.innerWidth / 110.8);
    // console.log(Math.floor(baseNumber));

  }

  // componentDidUpdate() {
  //   if(!this.props.remove.remove) {
  //     if(this.once === false) {
  //       this.once = true;
  //       this.translate = [-25, 0];
  //       this.opacity = [0,1];
  //     }
  //   }
  // }

  render() {

    let yo = (this.state.clicked % 2) === 0;
    var list = [];
    var i = 0;
    var j = 0;
    var baseNumber = Math.floor(window.innerWidth / 110.8);
    var rowLength = baseNumber;

    // console.log(baseNumber);

    if(this.once === false) {
      this.once = true;
      this.translate = [-25, 0];
      this.opacity = [0,1];
    }

    if(this.state.listItems !== null){
      for(let item in this.state.listItems) {

        if (j === rowLength) {

          if (rowLength === baseNumber - 1) {

            rowLength = baseNumber;
            list.push(this.create_(item, i, false, false));

          } else if (rowLength === baseNumber) {

            rowLength = baseNumber - 1;
            list.push(this.create_(item, i, true, false));

          }

          j = 0;

        } else if ((j === rowLength - 1) && (rowLength === baseNumber - 1)) {

          list.push(this.create_(item, i, false, true));

        } else {

          list.push(this.create_(item, i, false, false));

        }

        i++;
        j++;

      }
    }

    return (
      <div>
        <ul>
          {list}
        </ul>
      </div>
    );

  }

  parent_ = () => {

    const back = <a onClick={() => {}} class="neumorph card-full" onMouseUp={ this.handleEvent }>
      <FontAwesomeIcon
        icon="arrow-left"
      />
    </a>;
  }

  create_ = (k, delayl, lm, rm) => {

    // if(this.once === false) {
    //   this.once = true;
    //   this.translate = [-25, 0];
    //   this.opacity = [0,1];
    // }

    let keyChk = this.state.listItems.hasOwnProperty(k);

    if (keyChk) {

      const c = this.state.listItems[k].link.toString();
      const del = () => this.delete_(k);
      const pri = () => this.print_(c);

      const display = {
        left: lm,
        right: rm,
        delay: delayl,
        remove: this.props.remove.remove
      }

      return (
        <Link
          key={ k }
          item={ this.state.listItems[k] }
          display={ display }
          deleteFunc={ del }
          folderFunc={ pri }
        />
      );
    }
  }

  print_ = (c) => {

    this.props.setCurrent(c);
    this.current = c;
    var rootRef = fire.database().ref('folders/' + c);

    rootRef.on('value', (snapshot) => {

      var links = snapshot.val().links;

      if(this.current !== this.props.folder.root){
        links['0'] = {
          name: 'back',
          link: snapshot.val().parent,
          type: 'folder',
          icon: 'arrow-left'
        };
      }

      this.setState({listItems: links});

    });
  }

  delete_ = (c) => {
    // console.log(this.current);
    // console.log(c);

    var rootRef = fire.database().ref('folders/' + this.current + '/links');
    rootRef.child(c).remove();
  }

  // queuelearningmode (curr) {
  //   var childs = curr.children;
  //   while(!quit){
  //     var child = childs[i];
  //     makebutton(child);
  //     waitfornext();
  //   }
  // }

  spacer = () => {
    return (
      <li class='spacer'>
      </li>
    );
  }

}


const mapStateToProps = (state) => {
  return {
    remove: state.remove,
    folder: state.folder,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setCurrent: (current) => {
      dispatch(setCurrent(current));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Links);
