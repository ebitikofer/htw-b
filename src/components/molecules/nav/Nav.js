//core
import React, { Component } from 'react';
import { connect } from 'react-redux';

//config
import fire from '../../../config/Fire';
import Anime from '../../../components/utility/animations/Anime';
import {
  setHome,
  setUser,
  setLogin,
  setSubmit,
  setMusic
} from '../../../store/actions/appStateActions';
import { setRemove } from '../../../store/actions/removeActions';
import { setPomodoro } from '../../../store/actions/pomodoroActions';

//time
import Clock from '../../atoms/clock/Clock';

//style
import './Nav.css';

//images
// import logo from '../../res/logo.svg';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUserTie, faPlus } from '@fortawesome/free-solid-svg-icons'

class Nav extends Component {

  constructor(props) {

    super(props);

    this.once = false;
    this.add_once = false;

  }

  componentDidMount() {
    this.authListener();
  }

  //abstract if elses into render function

  // add() {
  //   if (this.state.submit === 1) {
  //     this.setState({ music: null, submit: null, login: null, remove: 1 });
  //   } else {
  //     this.setState({ music: null, submit: 1, login: null, remove: null });
  //   }
  //   localStorage.setItem('submit', true);
  // }

  render() {
    var translate = 0;
    var add_translate = 0;
    var log_opacity = 1;
    var add_opacity = 0;
    var icon_color = '#bb86fc';
    // var active_icon_color = 'var(--color-primary)';

    if (this.once === false) {
      this.once = true;
      translate = [-35, 0];
      log_opacity = [0, 1];
    }

    if (this.props.appState.user) {
      if (this.add_once === false) {
        this.add_once = true;
        add_translate = [-35, 0];
        add_opacity = [0, 1];
      }
    }
    
    if (this.props.pomodoro.pomodoro) {
      if (this.props.pomodoro.rest) {
        icon_color = '#3f5518'
      } else {
        icon_color = '#571d27';
      }
    }

    const content = this.props.appState.user
      ? this.props.appState.submit
        ? () => {
            this.props.setSubmit(!this.props.appState.submit);
            this.props.setRemove(!this.props.remove.remove);
          }
        : this.props.remove.remove
        ? () => {
            this.props.setRemove(!this.props.remove.remove);
          }
        : () => this.props.setSubmit(!this.props.appState.submit)
      : null;

    add_opacity = this.props.appState.user ? 1 : 0;

    // active_icon_color = this.props.appState.user
    // ? this.props.remove.remove
    //   ? '#cf6679'
    //   : '#bb86fc'
    // : '#3a2d49';

    const rotate = this.props.remove.remove ? 135 : 0;

    return (
      <div className="nav-container">

        {
        /* <div className="grid-item item2">
          <Anime
            easing="easeOutElastic"
            duration={1000}
            delay={6 * 62.5}
            translateY={translate}
            opacity={log_opacity}
          >
            <img
              src={arrowIcon}
              className="Nav-arrow"
              alt="arrow"
              onClick={this.logoClick}
            />
            <br />
          </Anime>
          <Anime
            easing="easeOutElastic"
            duration={1000}
            delay={7 * 62.5}
            translateY={translate}
            opacity={log_opacity}
          >
            <img
              src={logo}
              className="Nav-logo"
              alt="logo"
              onClick={this.logoClick}
            />
          </Anime>
        </div> */
        }

        <div className="grid-item item1" onClick={this.logoClick}>
          <Anime
            easing="easeOutElastic"
            duration={1000}
            delay={8 * 62.5}
            translateY={translate}
            opacity={log_opacity}>
            <p className="linkName">{this.props.folder.hover}</p>
          </Anime>
        </div>

        <div className="grid-item item2" onClick={this.logoClick}>
          <Anime
            easing="easeOutElastic"
            duration={1000}
            delay={8 * 62.5}
            translateY={translate}
            opacity={log_opacity}>
            <Clock offScreen={ this.props.offScreen } className="clock" />
          </Anime>
        </div>

        <div className="grid-item item3">
          <Anime
            easing="easeOutElastic"
            rotate={rotate}
            duration={1000}
            delay={5 * 62.5}
            translateY={add_translate}
            opacity={add_opacity}
            color={icon_color}
            // color={active_icon_color}
          >
            <div className="addIcon">
              <FontAwesomeIcon
                icon={faPlus}
                onClick={content}
              />
            </div>
          </Anime>
        </div>

        <div className="grid-item item4">
          <Anime
            easing="easeOutElastic"
            duration={1000}
            delay={9 * 62.5}
            translateY={translate}
            opacity={log_opacity}
            color={icon_color}>
            <div className="authIcon">
              <FontAwesomeIcon
                icon={faUserTie}
                onClick={this.logClick}/>
            </div>
          </Anime>
        </div>

      </div>
    );
  }

  logoClick = () => {
    this.props.setPomodoro(!this.props.pomodoro.pomodoro);
  }

  logClick = () => {
    this.props.appState.user
      ? fire.auth().signOut() && this.props.setUser(false)
      : this.props.setLogin(!this.props.appState.login);
  }

  authListener = () => {
    fire.auth().onAuthStateChanged(user => {
      if (user) {
        console.log(user.uid);
        localStorage.setItem('user', user.uid);
        this.props.setUser(true);
      } else {
        localStorage.removeItem('user');
      }
    });
  }

}

const mapStateToProps = state => {
  return {
    appState: state.appState,
    pomodoro: state.pomodoro,
    remove: state.remove,
    folder: state.folder
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setHome: home => {
      dispatch(setHome(home));
    },
    setUser: user => {
      dispatch(setUser(user));
    },
    setLogin: login => {
      dispatch(setLogin(login));
    },
    setSubmit: submit => {
      dispatch(setSubmit(submit));
    },
    setRemove: remove => {
      dispatch(setRemove(remove));
    },
    setPomodoro: pomodoro => {
      dispatch(setPomodoro(pomodoro));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Nav);
