//core
import React, { Component } from 'react';
import { connect } from 'react-redux';
//config
import fire from '../../../config/Fire';
import { setIndex, setTime } from '../../../store/actions/musicActions'
//style
import './Music.css';


class Music extends Component {

  constructor(props) {
    super(props);
    this.authListener = this.authListener.bind(this);
  }

  componentDidMount() {
    this.authListener();
    var rootRef = fire.database().ref('music');
    rootRef.on('value', (snapshot) => {
      this.props.setIndex(snapshot.val().index);
      this.props.setTime(snapshot.val().time);
    });
  }

  render() {
    // const yt = 'https://www.youtube.com/';
    // const embd = 'embed/videoseries';
    // const id = '?list=' + 'PLcI5DcePWMN0vZJUf5W4E_LW3e5ghyWGA';
    // const sttng = '&amp;showinfo=0&amp;rel=0&amp;autoplay=1';
    // const strt = '&amp;start=' + Math.ceil(Math.random() * this.props.music.time).toString();
    // const indx = '&amp;index=' + Math.ceil(Math.random() * (this.props.music.index - 1)).toString();
    //
    // const src = yt + embd + id + sttng + strt + indx;

    // return (
    //   <div>
    //     <iframe title="music" src={src} frameBorder="0" allow="autoplay; encrypted-media" height="50px" width="100%" allowFullScreen>mus</iframe>
    //   </div>
    // );

    return (
      <div>
      </div>
    );

  }

  authListener() {
    fire.auth().onAuthStateChanged((user) => {
      console.log(user.uid);
      if (user) {
        localStorage.setItem('user', user.uid);
        // this.setState({ submit: null, login: null, remove: null });
      } else {
        // this.setState({ user: null, login: null, submit: null, remove: null });
        localStorage.removeItem('user');
      }
    });
  }

}


const mapStateToProps = (state) => {
  return {
    music: state.music,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setIndex: (index) => {
      dispatch(setIndex(index));
    },
    setTime: (time) => {
      dispatch(setTime(time));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Music);
