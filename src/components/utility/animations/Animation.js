//core
import React, { Component } from 'react';
//config
import Anime from './Anime';
//style
import "./Animation.css"

class Animation extends Component {

  // constructor(props) {
  //   super(props);
  // }

  render() {

    let template = <div />;

    if (this.props.animation === null) {

      template = (

        <div className="animationWrapper">
          {this.props.children}
        </div>
        
      );

    } else {

      if (this.props.animation.firstRun === false) {

        this.props.animation.firstRunAnimation;

      }

      template = (

        <div className="animationWrapper">
          <Anime
            easing={this.props.animation.easing}
            duration={this.props.animation.duration}
            delay={1 * 62.5}
            translateY={this.props.animation.translateY}
            opacity={this.props.animation.opacity}
            boxShadow={this.props.animation.boxShadow}
          >
            <div>
              {this.props.children}
            </div>
          </Anime>
        </div>

      );

    }
    
    return template;

  }
  
}

export default Animation;
