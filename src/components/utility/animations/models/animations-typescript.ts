export default class animationSettings {
    easing?: string;
    rotate?: number;
    duration?: number;
    delay?: number;
    translateY?: number;
    opacity?: number;
    firstRun?: boolean;
};