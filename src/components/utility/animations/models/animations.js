export default class animationSettings {
    constructor(easing, rotate, duration, delay, translateY, opacity, firstRun) {
        this.easing = 'linear';
        this.rotate = 0;
        this.duration = 1000;
        this.delay = 0;
        this.translateX = 0;
        this.translateY = 0;
        this.opacity = 1;
        this.firstRun = false;
        this.height = 0;
        this.boxShadow = '';
        this.firstRunAnimation = function () {}
    }
};