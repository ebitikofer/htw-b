export function flipFlop(outerTrigger, innerTrigger, callbackOne, callbackTwo) {

    if (outerTrigger) {

        if (innerTrigger === false) {

            callbackOne();

        }

    } else {

        if (innerTrigger === true) {

            callbackTwo();

        }

    }

}